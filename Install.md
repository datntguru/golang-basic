```bash
export GOVERSION=1.20.2 && sudo rm -rf /usr/local/go && wget -O /tmp/go$GOVERSION.linux-amd64.tar.gz https://go.dev/dl/go$GOVERSION.linux-amd64.tar.gz && sudo tar -C /usr/local -xzf /tmp/go$GOVERSION.linux-amd64.tar.gz
```

1. **`wget https://go.dev/dl/go1.20.2.linux-amd64.tar.gz`** : Tải xuống tệp tin nén go1.20.2.linux-amd64.tar.gz từ trang web go.dev.
2. **`&&`** : Dấu && được sử dụng để chỉ thực hiện lệnh kế tiếp nếu lệnh trước đó thực hiện thành công.
3. **`sudo tar -C /usr/local -xzf go1.20.2.linux-amd64.tar.gz`** : Thực hiện giải nén tệp tin nén với tên go1.20.2.linux-amd64.tar.gz vào thư mục /usr/local bằng lệnh tar.

Bạn sẽ có được Go compiler trên hệ thống Linux của mình và có thể bắt đầu phát triển các ứng dụng Go.


Sẽ có 3 biến môi trường quan trọng mà các bạn cần chú ý đó là:

- **`PATH`**: trong Linux là một biến môi trường quan trọng để hệ thống có thể tìm kiếm các chương trình được cài đặt trên máy tính. Khi bạn nhập một lệnh trong Terminal, hệ thống sẽ tìm kiếm các thư mục được liệt kê trong biến PATH để tìm chương trình tương ứng với lệnh đó. Nếu biến PATH không được cấu hình đúng, hệ thống sẽ không tìm thấy chương trình và thông báo lỗi "command not found". Do đó, để đảm bảo hệ thống hoạt động đúng và hiệu quả, bạn cần cấu hình biến PATH một cách đúng đắn để bao gồm các đường dẫn đến các chương trình quan trọng mà bạn muốn sử dụng.
- **`GOROOT`**: là một biến môi trường trong Go, nó xác định đường dẫn đến thư mục gốc của Go compiler và các công cụ liên quan.
- **`GOPATH`**: là một biến môi trường trong Go, nó xác định đường dẫn đến thư mục làm việc của bạn khi phát triển các ứng dụng Go. Biến môi trường này quan trọng khi bạn muốn sử dụng các thư viện bên thứ ba hoặc chia sẻ mã nguồn của mình với những người khác.

```bash
sudo mkdir -p /opt/go/bin && sudo chown -R $USER:$USER /opt/go
```

```bash
export GOPATH=/opt/go
export GOROOT=/usr/local/go
export PATH=$PATH:$GOPATH/bin:$GOROOT/bin
```

Cụ thể, các lệnh được thực hiện như sau:

1. **`export`** : Đây là lệnh để thiết lập một biến môi trường trong Linux.
2. **`PATH=$PATH:/usr/local/go/bin`** : Câu lệnh này thêm đường dẫn của thư mục bin của Go compiler vào biến môi trường PATH.
- **`$PATH`** ở đây biểu thị cho giá trị hiện tại của biến môi trường PATH.
- **`:/usr/local/go/bin`** được thêm vào cuối để thêm đường dẫn của thư mục bin của Go compiler vào biến môi trường PATH.

Note: với câu lệnh trên thì nó chỉ active trong 1 tty session thôi, khi mà bạn tắt terminal đi thì biến môi trường của bạn cũng sẽ dc reset về default để mà bạn có thể lưu dc biến môi truờng bạn có thể chỉnh sữa file ~/.bashrc hoặc ~/.zshrc nếu bạn xài zsh. 

bạn có thể cài đặt nhiều version golang khác nhau bằng câu lệnh bên dưới đây:
```bash
go install golang.org/dl/go1.20.2@latest
```

Bonnus mình sẽ hướng dẫn các bạn cài vscode lun nhé
```bash
wget -O /tmp/vscode.deb https://code.visualstudio.com/sha/download\?build\=stable\&os\=linux-deb-x64 && sudo apt install /tmp/vscode.deb -y && rm /tmp/vscode.deb
```

```bash
while read -r line; do go install $line; done << EOF
github.com/cweill/gotests/gotests@latest
github.com/josharian/impl@latest
github.com/fatih/gomodifytags@latest
github.com/haya14busa/goplay/cmd/goplay@latest
github.com/go-delve/delve/cmd/dlv@latest
honnef.co/go/tools/cmd/staticcheck@latest
golang.org/x/tools/gopls@latest
github.com/ramya-rao-a/go-outline@latest
EOF
```

hello world go code:
```go
package main

import "fmt"

func main(){
  fmt.Println("Hello World!")
}
```